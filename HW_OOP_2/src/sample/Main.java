package sample;

public class Main {

	public static void main(String[] args) {
		Cat cat1= new Cat("mouse", "red", 4, "Umka");
		System.out.println(cat1);
		System.out.println(cat1.getVoice());
		cat1.eat();
		cat1.sleep();
		
		Dog dog1 = new Dog("meat", "black", 20, "Sharick");
		System.out.println(dog1);
		System.out.println(dog1.getVoice());
		dog1.eat();
		dog1.sleep();
		
		
		Veterinarian doc = new Veterinarian("Doc");
		doc.treatment(dog1);
		doc.treatment(cat1);
		
	}

}
