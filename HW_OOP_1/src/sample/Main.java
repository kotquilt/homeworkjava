package sample;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Product product1 = new Product("Mouse", "optical, small", 12.5, 100);
		Product product2 = new Product("Tomato", "red vegitable", 10.2, 1);
		
		System.out.println(product1);
		System.out.println(product2);
		
		
		Triangle tr1 = new Triangle (2.5, 3, 2.5);
		System.out.println(tr1);
		System.out.println(tr1.square());
		
		Triangle tr2 = new Triangle (0.3, 0.4, 0.5);
		
		System.out.println(tr2);
		System.out.println(tr2.square());
		
		
	}

}
