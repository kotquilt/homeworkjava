package sample;

public class Product {
	private String name;
	private String discription;
	private double price;
	private int weight;
	
	public Product (String name, String discription, double price, int weight) {
		super();
		this.name= name;
		this.discription = discription;
		this.price=price;
		this.weight=weight;
	}
	public Product() {
		super();
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDiscription() {
		return discription;
	}
	public void setDiscription(String discription) {
		this.discription = discription;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public int getWeight() {
		return weight;
	}
	public void setWeight(int weight) {
		this.weight = weight;
	};
	
	
	public String toString() {
		return "Product [name=" + name + ", discription= "+ discription+ ", price = " + price +", weight=" + weight + "]";
	}
	
	
	
}