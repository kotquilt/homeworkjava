package sample;

public class Main {

	public static void main(String[] args) {
		double sideA = 0.3;
		double sideB = 0.4;
		double sideC = 0.5;
		double square;
		double p;
		p = (sideA+sideB+sideC)/2;
		square = Math.sqrt(p*(p-sideA)*(p-sideB)*(p-sideC));
		System.out.println("Triangle square = "+ square);
		
		int applePrice =2;
		int amount = 6;
		int cost;
		cost = applePrice*amount;
		System.out.println("Total cost, $:" + cost);
		
		double fuelPrice = 1.2;
		double distance = 120;
		double fuelConsumption = 8;
		double consumptionDistance =100; 
		double fuelCost;
		fuelCost= fuelPrice*distance*(fuelConsumption/consumptionDistance);
		System.out.println("Fuel cost for trip 120 km, $:" + fuelCost);

	}

}
