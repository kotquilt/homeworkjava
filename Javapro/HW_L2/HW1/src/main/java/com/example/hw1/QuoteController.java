package com.example.hw1;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@RestController
public class QuoteController {
    @GetMapping("/quote")
    public String quote(){
        List <String> quotes=new ArrayList<>();
        quotes.add("Off with their heads!");
        quotes.add("Why, sometimes I've believed as many as six impossible things before breakfast");
        quotes.add("It's no use going back to yesterday, because I was a different person then.");
        quotes.add("We're all mad here.");

        Random rn = new Random();
        String rnQuote = quotes.get(rn.nextInt(quotes.size()));
        return rnQuote;
    }

}
