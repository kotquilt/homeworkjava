package sample;

import java.util.ArrayList;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

public class Group {
	private String groupName;
	private List<Student> students;
	
	public Group(String groupName) {
		super();
		this.groupName = groupName;
		this.students=new ArrayList<>(10);
		
	}
	


	public String getGroupName() {
		return groupName;
	}



	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}



	public List<Student> getStudents() {
		return students;
	}



	public void setStudents(List<Student> students) {
		this.students = students;
	}



	public void addStudent(Student student) {
		if(students.size()<10) {
			students.add(student);			
		}else {
			System.out.println("To much students in the group");
	}return;
	}
	public Student searchStudentByLastName(String lastName) throws StudentNotFoundException {
		for (Student element:students) {
			if (element.getLastName().equals(lastName)) {
					return element;
				}
			}
		throw new StudentNotFoundException("There no student with last name" +lastName + "in the Group" + groupName + ".");
	}
	
	public boolean removeStudentByID(int id) {
		for (Student element:students) {
			if (element.getId()==id) {
					return true;
				}
				
			} return false;
	}
	
	public void sortStudentsByLastName() {
		Collections.sort(students, Comparator.nullsFirst(new StudentSortComparator()));
	    System.out.println();
	    for (Student element:students) {
		      System.out.println(element);
	    }
	    
	    
	}
	public boolean equalsStudents() {
		Object [] array= new Object[students.size()];
		students.toArray(array);
		for (int i = 0; i < array.length; i++) {
				for (int j = i+1; j < array.length; j++) {
					if (array[i]!=null && array [j]!= null && array[i].equals(array[j])) {
						System.out.println(array[i]+" equals "+ array[j]);
						return true;
					}
				}
			}return false;	
		

		
	}

	@Override
	public String toString() {
		
		String result = "Group "+ groupName + " students list: " + System.lineSeparator();
		for (Student element:students) {
			result +=element+System.lineSeparator();
					}
		return result ;
	}


	@Override
	public int hashCode() {
		return Objects.hash(groupName, students);
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Group other = (Group) obj;
		return Objects.equals(groupName, other.groupName) && Objects.equals(students, other.students);
	}



	

		
	
	
	
}
