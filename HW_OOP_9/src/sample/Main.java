package sample;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Student student1 = new Student("J", "J", Gender.Male, 1, "First");
		Student student2 = new Student("B", "B", Gender.Male, 6, "First");
		Student student3 = new Student("C", "C", Gender.Male, 2, "First");
		Student student4 = new Student("J", "J", Gender.Male, 1, "First");
		Student student5 = new Student("A", "A", Gender.Male, 4, "First");
		Student student6 = new Student("F", "F", Gender.Male, 5, "First");
		
		
		
		Group group1 = new Group("First");
		
			group1.addStudent(student1);
			group1.addStudent(student2);
			group1.addStudent(student3);
			group1.addStudent(student4);
			group1.addStudent(student5);
			
			group1.addStudent(student6);
			
			
			
		try {
			System.out.println(group1.searchStudentByLastName("Petrov"));
		} catch (StudentNotFoundException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
		System.out.println(group1);
		System.out.println(group1.removeStudentByID(3));
		System.out.println(group1);
	
		group1.sortStudentsByLastName();
		
//		Student studentNew = new CreateStudent().newStudent();
//		System.out.println(studentNew);
//		
//		CreateStudent.addStudentToGroup(studentNew, group1);
//		System.out.println(group1);
		
		System.out.println(student1.equals(student2));
		System.out.println(student1.hashCode());
		System.out.println(student2.hashCode());
		Group group2= new Group("Second");
		Student st1 = new Student("a", "aa", Gender.Male, 10, "Second");
		group2.addStudent(st1);
		
		System.out.println(group2);
		System.out.println(group1.equals(group2));
		System.out.println(group1.hashCode());
		System.out.println(group2.hashCode());
		System.out.println("-----------------------");
		System.out.println(group1.equalsStudents());
	}

}
