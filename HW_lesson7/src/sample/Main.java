package sample;

import java.util.Arrays;
import java.util.Random;

public class Main {

	public static void main(String[] args) {
		Random rn = new Random ();
		int [] arr1 = new int [] {2, 5, 10, 25, 60};
		int maxInt=maxInt(arr1);
		System.out.println(maxInt);
		
		int height = 3;
		int width = 5;
		rectangle(height, width);
		
		int [] arr2 = new int[] {2, 6, 7, -2, 8, -10, 5};
		
		int f =5;
		
		int linearSearch = linearSearch(f, arr2);
		System.out.println(linearSearch);
		
		
	}
	
	public static int maxInt (int [] array) {
		int maxInt=array[0];
		for (int i=0; i<array.length; i++) {
			if (array[i]>maxInt) {
				maxInt=array[i];
			}
		}
		return maxInt;
	}; 
	public static void rectangle (int h, int w) {
		for (int i=1; i<=h; i++) {
			for (int j=1; j<w; j++) {
			System.out.print("*");
			}
			System.out.println("*");
		}
	};
	
	public static int linearSearch (int f, int [] array ) {
		for (int i=0; i<array.length; i++) {
			if (array[i] == f) {
				return i;
			} 
		}return -1;
	};

}
