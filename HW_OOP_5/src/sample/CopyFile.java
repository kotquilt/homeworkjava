package sample;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;

public class CopyFile {
	
	public static long copyFile(File fileIn, File fileOut) throws IOException {
		try (Reader is = new FileReader(fileIn)) {
			Writer os = new FileWriter(fileOut);
			return is.transferTo(os);
			
		} 
		
	}

	public static void copyFiles(File folderIn, File folderOut, String extension) throws IOException {
		File [] files = folderIn.listFiles();
		for(int i = 0; i< files.length; i++ ) {
			if(files[i].isFile()) {
		        File fileOut = new File(folderOut, files[i].getName());
		        if (files[i].getName().endsWith(extension)) {
		        System.out.println(copyFile(files[i], fileOut));
		      }
		    }
		}
	}
}
