package sample;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Student student1 = new Student("Ivan", "Ivanov", Gender.Male, 0, "First");
		Student student2 = new Student("Ivan", "Petrov", Gender.Male, 1, "First");
		Student student3 = new Student("Ivan", "Sidorov", Gender.Male, 2, "First");
		Student student4 = new Student("Ivan", "Valenok", Gender.Male, 3, "First");
		Student student5 = new Student("Ivan", "Ivanov", Gender.Male, 4, "First");
		Student student6 = new Student("Ivan", "Ivanov", Gender.Male, 5, "First");
		Student student7 = new Student("Ivan", "Ivanov", Gender.Male, 6, "First");
		Student student8 = new Student("Ivan", "Ivanov", Gender.Male, 7, "First");
		Student student9 = new Student("Ivan", "Ivanov", Gender.Male, 8, "First");
		Student student10 = new Student("Ivan", "Ivanov", Gender.Male, 9, "First");
		Student student11= new Student("Ivan", "Ivanov", Gender.Male, 10, "First");
		Student student12= new Student("Ivan", "Ivanov", Gender.Male, 11, "First");
		
		
		Group group1 = new Group("First");
		try {
			group1.addStudent(student1);
			group1.addStudent(student2);
			group1.addStudent(student3);
			group1.addStudent(student4);
			group1.addStudent(student5);
			
			group1.addStudent(student6);
			group1.addStudent(student7);
			group1.addStudent(student8);
			group1.addStudent(student9);
			group1.addStudent(student10);
			
		} catch (GroupOverflowException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
		try {
			System.out.println(group1.searchStudentByLastName("Petrov"));
		} catch (StudentNotFoundException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
		System.out.println(group1);
		System.out.println(group1.removeStudentByID(3));
		System.out.println(group1);
	
		group1.sortStudentsByLastName();
		
		Student studentNew = new CreateStudent().newStudent();
		System.out.println(studentNew);
		
		CreateStudent.addStudentToGroup(studentNew, group1);
		System.out.println(group1);
		

	}

}
