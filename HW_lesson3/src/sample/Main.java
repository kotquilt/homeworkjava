package sample;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int number1;
		int number2;
		int number3;
		int number4;
		int maxNumber;
		System.out.println("Insert first number");
		number1 = sc.nextInt();
		System.out.println("Insert second number");
		number2 = sc.nextInt();
		System.out.println("Insert third number");
		number3 = sc.nextInt();
		System.out.println("Insert fourth number");
		number4 = sc.nextInt();

		maxNumber = number1;
		if (number2 > maxNumber) {
			maxNumber = number2;
		}
		if (number3 > maxNumber) {
			maxNumber = number3;
		}
		if (number4 > maxNumber) {
			maxNumber = number4;
		}
		System.out.println("Max Number:" + maxNumber);

		int floorsNumber = 9;
		int entrancesNumber = 5;
		int flatsPerFloor = 4;
		int maxFlatNumber = floorsNumber * entrancesNumber * flatsPerFloor;
		int flatsPerEntrance = maxFlatNumber / entrancesNumber;
		int flat;
		int flatsEntr;
		int flatsFloor;
		
		System.out.println("Insert flat number");
		flat = sc.nextInt();
		if (flat > 0 && flat <= maxFlatNumber) {
			if (flat % flatsPerEntrance == 0) {
				flatsEntr = flat / flatsPerEntrance;
			} else {
				flatsEntr = flat / flatsPerEntrance + 1;
			};
			

			if (flat%flatsPerFloor==0 && flat%flatsPerEntrance!=0) {
				flatsFloor=flat%flatsPerEntrance/flatsPerFloor;
				
			} else if (flat%flatsPerEntrance==0) {
				flatsFloor=flat/flatsEntr/flatsPerFloor;
			}else {
				flatsFloor=flat%flatsPerEntrance/flatsPerFloor+1;
			};

			System.out.println("Flat "+flat+" on "+flatsFloor+ " floor, in "+flatsEntr+" entrance");

			
		} else {
			System.out.println("There no such flat in this house");
		};

		
		
		int a;
		int b;
		int c;
		System.out.println("Insert sade a");
		a = sc.nextInt();
		System.out.println("Insert sade b");
		b = sc.nextInt();
		System.out.println("Insert sade c");
		c = sc.nextInt();
		if ((a + b) > c && (a + c) > b && (b + c) > a) {
			System.out.println("Tiangle exist");
		} else {
			System.out.println("Triangle don't exist");
		}
	}

}
