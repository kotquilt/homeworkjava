package sample;

public interface StringConvertor {
	
	public String toStringRepresentation(Student student); 
	public Student fromStringRepesentation (String str);
}
