package sample;

public class SVGStringConvertor implements StringConvertor {

	@Override
	public String toStringRepresentation(Student student) {
		String result = student.getName() + ", "+ student.getLastName()+", "+student.getGender()+", "+student.getId()+ ", "+student.getGroupName();
		return result;
	}

	@Override
	public Student fromStringRepesentation(String str) {
		String [] studentArr = str.split(", ");
		Student student = new Student(studentArr[0], studentArr[1], setStudentGender(studentArr[2]), Integer.parseInt(studentArr[3]), studentArr[4]);
		return student;
	}
	
	private Gender setStudentGender(String gender) {
		for (int i = 0; i<Gender.values().length; i++) {
			if (Gender.values()[i].toString().equals(gender)) {
				return Gender.values()[i];
			}
		}
		return null;
		
	}
	

}
