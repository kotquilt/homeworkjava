package sample;

import java.awt.image.ColorConvertOp;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

import java.util.Scanner;

public class GroupFileStorage {
	public static void saveGroupToCSV(Group gr) {
		
		try (PrintWriter groupScv = new PrintWriter(new File(gr.getGroupName()+".svg"))) {
			for (int i=0; i<gr.getStudents().length; i++) {
			groupScv.println(gr.getStudents()[i]);
			} 
		}catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
	}
	
	public static Group loadGroupFromCSV (File file) throws IOException, GroupOverflowException{
		SVGStringConvertor cr = new SVGStringConvertor();
		Group tmpGroup =new Group(file.getName());
		try (Scanner sc = new Scanner(file)){
			for (; sc.hasNextLine();) {
			tmpGroup.addStudent( cr.fromStringRepesentation(sc.nextLine()+System.lineSeparator())); 
				 			
				}
			
			
		}
		
		return tmpGroup;
	}
	 
	
	public static File findFileByGroupName(String groupName, File workFolder) throws IOException{
		
		File [] files = workFolder.listFiles();
		if (files!=null) {
			for (File element:files) {
				if(element.isFile()&&element.getName().equals(groupName + ".svg")) {
					return element;
				}
			}
			
		} return null;
	}
	
	

		
	
}


	
