package sample;

import java.io.File;
import java.io.IOException;

public class Main {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		
		Student student1 = new Student("Ivan", "Ivanov", Gender.Male, 0, "First");
		Student student2 = new Student("Ivan", "Petrov", Gender.Male, 1, "First");
		Student student3 = new Student("Ivan", "Sidorov", Gender.Male, 2, "First");
		Student student4 = new Student("Ivan", "Valenok", Gender.Male, 3, "First");
		Student student5 = new Student("Ivan", "Ivanov", Gender.Male, 4, "First");
		Student student6 = new Student("Ivan", "Ivanov", Gender.Male, 5, "First");
		Student student7 = new Student("Ivan", "Ivanov", Gender.Male, 6, "First");
		Student student8 = new Student("Ivan", "Ivanov", Gender.Male, 7, "First");
		Student student9 = new Student("Ivan", "Ivanov", Gender.Male, 8, "First");
		Student student10 = new Student("Ivan", "Ivanov", Gender.Male, 9, "First");
		Student student11= new Student("Ivan", "Ivanov", Gender.Male, 10, "First");
		Student student12= new Student("Ivan", "Ivanov", Gender.Male, 11, "Second");
		
		
		Group group1 = new Group("First");
		try {
			group1.addStudent(student1);
			group1.addStudent(student2);
			group1.addStudent(student3);
			group1.addStudent(student4);
			group1.addStudent(student5);
			
			group1.addStudent(student6);
			group1.addStudent(student7);
			group1.addStudent(student8);
			group1.addStudent(student9);
			group1.addStudent(student10);
			
		} catch (GroupOverflowException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
		try {
			System.out.println(group1.searchStudentByLastName("Petrov"));
		} catch (StudentNotFoundException e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
		System.out.println(group1);
		System.out.println(group1.removeStudentByID(3));
		System.out.println(group1);
	
		group1.sortStudentsByLastName();
		
		Group group2 = new Group("Second.svg");
		try {
			group2.addStudent(student11);
			group2.addStudent(student12);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
		Student student13 = new CreateStudent().newStudent();
		System.out.println(student13);
	
		CreateStudent.addStudentToGroup(student13, group2);
		System.out.println(group1);
		
		
		GroupFileStorage.saveGroupToCSV(group1);
			
		
		GroupFileStorage.saveGroupToCSV(group2);
		
		
		
		File workFolder = new File (".");
		String searchGroup = ("Second.svg");
		File gr = GroupFileStorage.findFileByGroupName(searchGroup, workFolder);
		if (gr!=null) {
			System.out.println("Group " + searchGroup + " is exist");
		} else {
			System.out.println("Group " + searchGroup + " is not exist");
		}
		

	}

}
