package sample;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner sr = new Scanner(System.in);
		int n;
		System.out.println("Input nomber n, 4<n<16");
		n = sr.nextInt();
		int factorial = 1;
		if (n > 4 && n < 16) {
			for (int i = 1; i <= n; i++) {
				factorial = factorial * i;
			}

			System.out.println("Factorial number " + n + " = " + factorial);
		} else {
			System.out.println("Try again");
		}
		;

		int multiplier = 5;
		int sum;
		for (int i = 1; i <= 10; i++) {
			sum = i * multiplier;
			System.out.println(i + "x" + multiplier + "=" + sum);
		}
		;

		int height;
		int width;
		System.out.println("Input height");
		height=sr.nextInt();
		System.out.println("Input width");
		width=sr.nextInt();
		for (int i = 1; i <= height; i++) {
			if (i == 1 || i == height) {
				for (int j = 1; j <= width; j++) {
					System.out.print("*");
				}
			} else {
				for (int j = 1; j <= width; j++) {
					if (j == 1 || j == width) {
						System.out.print("*");
					} else {
						System.out.print(" ");
					}
				}
			}
			System.out.println();
		}

	}

}
