package sample;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

public class Main {

	public static void main(String[] args) {
		File editor = new File ("editor.txt");
		try (PrintWriter pw1= new PrintWriter(editor)) {
			pw1.println("Hello world");
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		int[][] array = new int[][] { { 1, 2 }, { 3, 4 }, { 5, 6 } };
		File arr = new File ("array.txt");
		saveArray(arr, array);
		
		File dirr = new File (".");
		showFolder(dirr);
		
		
	};
	
	public static void saveArray(File arr, int[][] array) {
		try (PrintWriter pw2 = new PrintWriter(arr)) {
			
			for (int[] row : array) {
				for (int element : row) {
					pw2.println(element);
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	};
	
	public static void showFolder(File dirr) {
		File [] files = dirr.listFiles();
		for (File element:files) {
			if (element.isDirectory()) {
				System.out.println(element);	
			}
			
		}
	}
}
