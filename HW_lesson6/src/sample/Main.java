package sample;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner sc = new Scanner (System.in);
		System.out.println("Input text");
		String text = sc.nextLine();
		String tempText=text.toLowerCase();
		int n=0;
		char [] sym = tempText.toCharArray();
		for (int i=0; i<sym.length; i++) {
			if (sym[i]=='b') {
				n +=1;
			}
		};
		System.out.println("In this text "+ n+ " letters b");
		System.out.println("Input string with several words");
		String text2=sc.nextLine();
		int n1=0;
		String maxWord = null;
		String[] result = text2.split("[ ]");
		for (int i= 0; i<result.length; i++) {
			int j=result[i].length();
			if (j>n1) {
				n1=j;
				maxWord=result[i];
			}
		}
		System.out.println(maxWord);
		for(int i=2; i<=10; i++) {
		String pi=String.format("PI number value %." + i+"f", Math.PI);
		System.out.println(pi);}

	}

}
